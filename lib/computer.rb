class Computer
  attr_accessor :mark, :opponent_mark, :best_move

  def initialize(mark)
    @mark = mark
    if @mark == 'X'
      @opponent_mark = 'O'
    else
      @opponent_mark = 'X'
    end
    @best_move = 0
  end

  # retrieves computer's move
  def get_move(board_obj)
    puts <<-EOS.gsub(/^ */, '')

    <<computer>> BleEP, BLoop, beep
    EOS
    negamax(board_obj, @mark, 1)
    board_obj.empty_squares
    @best_move
  end

  # ham-fisted negamax
  def negamax(board_obj, mark, depth)
    if board_obj.game_over?
      return value(board_obj, mark)
    else
      max = -1.0 / 0 # negative infinity
      if mark == @mark
        next_mark = @opponent_mark
      else
        next_mark = @mark
      end
      board_obj.empty_squares.each do |square|
        board_obj[square] = mark
        score = -negamax(board_obj, next_mark, depth + 1)
        board_obj[square] = ' '
        if score > max
          max = score
          @best_move = square if depth == 1
        end
      end
      max
    end
  end

  # determines value of final board state
  def value(board_obj, mark)
    if board_obj.mark_win?(mark)
      return 1
    elsif  board_obj.mark_win?(mark == 'X' ? 'O' : 'X')
      return -1
    else
      return 0
    end
  end
end
