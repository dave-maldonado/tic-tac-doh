# main game logic
class Game
  attr_accessor :current_player, :board, :player, :computer

  WELCOME = <<-'EOS'.gsub(/^ */, '')
  Welcome to...
  .        _____ _       _____            ___   ___  _  _  _
  .       |_   _(_)__ __|_   _|_ _ __ ___|   \ / _ \| || | |
  .         | | | / _|___|| |/ _` / _|___| |) | (_) | __ |_|
  .         |_| |_\__|    |_|\__,_\__|   |___/ \___/|_||_(_)
  .
  .
  Hello brave player! Would you like to play as the
  indomitable 'X'? or the crafty 'O'?
  EOS

  KEY = <<-EOS.gsub(/^ */, '')

  |0|1|2|
  |3|4|5|
  |6|7|8|
  EOS

  def initialize
    @current_player = :human
  end

  # starts game and prompts player to choose mark
  def start_game
    puts WELCOME
    @board = Board.new
    get_mark
    puts <<-EOS.gsub(/^ */, '')

    Let us begin!

    EOS
    board.print_board
    turn
  end

  # get player's mark
  def get_mark
    good_mark = false
    until good_mark
      mark = gets.chomp
      if mark =~ /x/i
        chose_x
        good_mark = true
      elsif mark =~ /o/i
        chose_o
        good_mark = true
      else
        puts <<-EOS.gsub(/^ */, '')

        What is this strange mark? please choose 'X' or 'O'!
        EOS
      end
    end
  end

  def chose_x
    puts <<-EOS.gsub(/^ */, '')

    well chosen!, you shall be 'X'
    EOS
    @player = Player.new('X')
    @computer = Computer.new('O')
  end

  def chose_o
    puts <<-EOS.gsub(/^ */, '')

    interesting choice, you shall be 'O'
    EOS
    @player = Player.new('O')
    @computer = Computer.new('X')
  end

  # uses @current_player var to switch turns between player and computer
  # checks game completion at end of each turn
  def turn
    puts KEY
    case current_player
    when :human
      human_move
    when :computer
      computer_move
    end
    turn
  end

  # players section of turn
  def human_move
    good_move = false
    until good_move
      move = player.get_move.to_i
      if board.square_blank?(move)
        good_move = true
      else
        puts <<-EOS.gsub(/^ */, '')

        sorry that square is taken...
        EOS
        board.print_board
        puts KEY
      end
    end
    board[move] = @player.mark
    board.print_board
    game_end if board.game_over?
    @current_player = :computer
  end

  # computer's section of turn
  def computer_move
    move = @computer.get_move(@board)
    @board[move] = @computer.mark
    board.print_board
    game_end if board.game_over?
    @current_player = :human
  end

  # prints message when game is over and triggers play_again
  def game_end
    if board.game_won? == true
      case @current_player
      when :human
        puts <<-EOS.gsub(/^ */, '')

        you won! :)
        EOS
        play_again
      when :computer
        puts <<-EOS.gsub(/^ */, '')

        the computer won... :(
        EOS
        play_again
      end
    else
      puts <<-EOS.gsub(/^ */, '')

      a draw! :/
      EOS
      play_again
    end
  end

  # prompts player to choose replay or exit
  def play_again
    puts <<-EOS.gsub(/^ */, '')

    Do you want to play again?
    (y/n)
    EOS
    choice = gets.chomp
    if choice =~ /yes|y/i
      puts ''
      load 'tic-tac-doh'
    else
      Process.exit
    end
  end
end
