# represents game-play board
class Board
  attr_accessor :board, :solutions

  def initialize
    @board = Array.new(9) { ' ' }
    @solutions = []
  end

  # retrieves square
  def [](square)
    @board[square]
  end

  # writes to square
  def []=(square, mark)
    @board[square] = mark
  end

  # make array of board solutions
  def make_solutions
    rows = [@board[0..2], @board[3..5], @board[6..8]]
    columns = rows.transpose
    l_diagonal = [@board[0], @board[4], @board[8]]
    r_diagonal = [@board[2], @board[4], @board[6]]
    rows.map { |row| @solutions << row }
    columns.map { |col| @solutions << col }
    @solutions << l_diagonal << r_diagonal
  end

  # tests if cell is blank
  def square_blank?(square)
    @board[square] == ' '
  end

  # return array of empty squares indices
  def empty_squares
    empty_squares = []
    @board.each_with_index do |square, index|
      empty_squares << index if square == ' '
    end
    empty_squares
  end

  # prints board to console
  def print_board
    puts <<-EOS.gsub(/^ */, '')

    |#{@board[0]}|#{@board[1]}|#{@board[2]}|
    |#{@board[3]}|#{@board[4]}|#{@board[5]}|
    |#{@board[6]}|#{@board[7]}|#{@board[8]}|
    EOS
  end

  # tests if game has been won
  def game_won?
    @solutions.clear
    make_solutions
    won = false
    @solutions.each do |solution|
      if solution[0] != ' ' &&
          solution[0] == solution[1] && solution[1] == solution[2]
        won = true
      end
    end
    won
  end

  # tests if game was won with specific mark
  def mark_win?(mark)
    mark_won = false
    @solutions.each do |solution|
      mark_won = true  if solution == [mark, mark, mark]
    end
    mark_won
  end

  # tests if game is a draw
  def game_draw?
    empty_squares == []
  end

  # tests if game is over
  def game_over?
    game_won? || game_draw?
  end
end
