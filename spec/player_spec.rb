require_relative 'spec_helper'

describe Player do
  before(:each) do
    @player = Player.new('X')
  end

  describe "#new" do
    it "returns new Player object" do
      @player.should be_an_instance_of Player
    end
  end

  describe "#get_move" do
    it "gets players input" 
    it "prompts player if input is bad" 
  end
end
