$LOAD_PATH.unshift 'lib'
require "tic-tac-doh/version"

Gem::Specification.new do |s|
  s.name              = "tic-tac-doh"
  s.version           = TIC_TAC_DOH::VERSION
  s.date              = Time.now.strftime('%Y-%m-%d')
  s.summary           = "an unbeatable console game"
  s.homepage          = "http://github.com//tic-tac-doh"
  s.email             = "david.m.maldonado@gmail.com"
  s.authors           = [ "David Maldonado" ]
  s.has_rdoc          = false
  s.rubyforge_project = "tic-tac-doh"

  s.files             = %w( Readme.md Rakefile)
  s.files            += Dir.glob("lib/**/*")
  s.files            += Dir.glob("bin/**/*")
  s.test_files        = Dir.glob("spec/**/*")
  s.executables       = %w(tic-tac-doh)
  s.description       = <<desc
  an unbeatable console game
desc

  s.add_development_dependency 'rake'
  s.add_development_dependency 'rspec'
end
