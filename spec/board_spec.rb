require_relative 'spec_helper'

describe Board do
  before(:each) do
    @board = Board.new
  end

  describe "#new" do
    it "returns new Board object" do
      @board.should be_an_instance_of Board
    end
  end

  describe "#[]" do
    it "accesses square on board array" do
       (0..8).each do |index|
        @board[index].should equal @board.board[index]
      end
    end
  end

  describe "#[]=" do
    it "sets specified square" do
      (0..8).each do |index|
        @board[index] = index
        @board[index].should equal index
      end
    end
  end

  describe "#make_solutions" do
    it "creates array of size 8" do
      @board.make_solutions
      @board.should have(8).solutions
    end

    it "creates array that matches winning solutions" do
      @board.board = ['0', '1', '2', '3', '4', '5', '6', '7', '8']
      @board.make_solutions
      rows = [@board[0..2], @board[3..5], @board[6..8]]
      columns = rows.transpose
      l_diagonal = [@board[0], @board[4], @board[8]]
      r_diagonal = [@board[2], @board[4], @board[6]]
      @board.solutions.should == [rows[0], rows[1], rows[2],
                                    columns[0],columns[1], columns[2],
                                    l_diagonal, r_diagonal]
    end
  end

  describe "#square_blank?" do
    it "checks if square is blank" do
      @board.board = ['0', ' ', '']
      @board.square_blank?(0).should == false
      @board.square_blank?(1).should == true
      @board.square_blank?(2).should == false
    end
  end

  describe "#empty_squares" do
    it "returns array of empty squares on board" do
      @board.board = ['0', ' ', '0']
      @board.empty_squares.size.should == 1
      @board.empty_squares[0].should == 1
    end
  end


  describe "#print_board" do
    it "prints out the current game board" do
      expect {
        puts <<-EOS.gsub(/^ */, '')

        |#{@board[0]}|#{@board[1]}|#{@board[2]}|
        |#{@board[3]}|#{@board[4]}|#{@board[5]}|
        |#{@board[6]}|#{@board[7]}|#{@board[8]}|
        EOS
      }
      end
   end

  describe "#game_won?" do
    it "returns true if game has been won" do
      @board.board = ['X', 'X', 'X', ' ', ' ', ' ', ' ', ' ', ' ']
      @board.game_won?.should == true
    end

    it "returns false if game hasn't been won" do
      @board.board = [' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ']
      @board.game_won?.should == false
    end
  end

  describe "#game_draw?" do
    it "returns true if game is a draw" do
      @board.board = ['X', 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X']
      @board.empty_squares
      @board.game_draw?.should == true
    end

    it "returns false if game is not a draw" do
      @board.board = [' ', ' ', 'X', 'O', 'X', 'O', ' ', ' ', ' ']
      @board.empty_squares
      @board.game_draw?.should == false
    end
  end

  describe "#game_over?" do
    it "returns false if a board is blank" do
      @board.game_over?.should == false
    end

    it "returns false if a board is still in play" do
      @board.board = ['X', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ']
      @board.game_over?.should == false
    end

    it "returns true if a player wins" do
      @board.board = ['X', 'X', 'X', ' ', ' ', ' ', ' ', ' ', ' ']
      @board.game_over?.should == true
    end

    it "returns true if game is a draw" do
      @board.board = ['X', 'X', 'X', 'X', 'X', 'X', 'X', 'X', 'X']
      @board.game_over?.should == true
    end
   end
 end
