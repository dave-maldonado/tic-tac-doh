# representation of human player
class Player
  attr_accessor :mark

  def initialize(mark) # 'X' or 'O'
    @mark = mark
  end

  # retrieves players desired move
  def get_move
    puts <<-EOS.gsub(/^ */, '')

    make your move!
    EOS
    while true
      square = gets.chomp # prompt player for move
      if square =~ /^[0-8]$/
        return square
      else
        puts <<-EOS.gsub(/^ */, '')

        um, not sure what you mean - use 0 thru 8 to
        tell me which square you want to mark.
        EOS
      end
    end
  end
end
