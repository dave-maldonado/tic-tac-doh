require_relative 'spec_helper'

describe Computer do
  before(:each) do
    @computer = Computer.new('X')
  end

  describe "#new" do
    it "returns new Computer object" do
      @computer.should be_an_instance_of Computer
    end

    it "initializes mark correctly" do
      @computer.mark.should == 'X'
    end

    it "initializes opponent_mark correctly" do
      @computer.opponent_mark.should == 'O'
      @computer = Computer.new('O')
      @computer.opponent_mark.should == 'X'
    end

    it "set best_move to 0" do
      @computer.best_move.should == 0
    end
  end

  describe "#get_move" do
    it "calls negamax method" do
      @computer.should_receive(:negamax)
      @computer.get_move(board = Board.new)
    end
  end
  
  describe "#negamax" do
    # woah, no idea how to test this :(
    it "determines the optimum move for the AI"
  end

  describe "#value" do
    it "returns 1 if mark wins" do
      board = Board.new
      board.board = ['X', 'X', 'X', ' ', ' ', ' ', ' ', ' ', ' ']
      board.make_solutions
      @computer.value(board, 'X').should == 1
    end

    it "returns -1 if opponent mark wins" do
      board = Board.new
      board.board = ['O', 'O', 'O', ' ', ' ', ' ', ' ', ' ', ' ']
      board.make_solutions
      @computer.value(board, 'X').should == -1
    end

    it "returns 0 if draw" do
      board = Board.new
      board.board = ['X', 'O', 'X', 'O', 'X', 'O', 'O', 'X', 'O']
      board.make_solutions
      @computer.value(board, 'X').should == 0
    end
  end
end
