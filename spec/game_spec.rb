require_relative 'spec_helper'

describe Game do
  before(:each) do
    @game = Game.new
  end

  describe "#new" do
    it "returns new Game object" do
      @game.should be_an_instance_of Game
    end

    it "initializes @current_player to :human" do
      @game.current_player.should equal :human
    end
  end

  describe "#start_game" do
    it "prints welcome message" do
      expect {
        @game::WELCOME
      }
    end
        
    it "prints 'Let us begin!'" do
      expect {
        puts <<-EOS.gsub(/^ */, '')

        Let us begin!

        EOS
      }
    end
  end
  
  describe "#get_mark" do
    it "calls chose_x if player chooses 'X'" 
   
    
    it "calls chose_o if player chooses 'O'"
  end

  describe "#chose_x" do
    it "prints confirmation" do
      @game.chose_x
      expect {
        puts <<-EOS.gsub(/^ */, '')

        well chosen!, you shall be 'X'
        EOS
      }
    end

    it "creates new Player object" 
    
    it "creates new Computer object"
    
    it "sets @player to 'X'" do
      @game.chose_x
      @game.player.mark.should == 'X'
    end

    it "sets @computer to 'Y'" do
      @game.chose_x
      @game.computer.mark.should == 'O'
    end
  end

  describe "#chose_o" do
    it "prints confirmation" do
      @game.chose_o
      expect {
        puts <<-EOS.gsub(/^ */, '')

        interesting choice, you shall be 'O'
        EOS
      }
    end
    
    it "creates new Player object"
    
    it "creates new Computer object"
    
    it "sets @player to 'O'" do
      @game.chose_o
      @game.player.mark.should == 'O'
    end

    it "sets @computer to 'X'" do
      @game.chose_o
      @game.computer.mark.should == 'X'
    end
  end
    
  describe "#turn" do
    it "prints key/legend for board" do
      expect {
        puts Game::KEY
      }
    end
    
    it "calls human_move if it's the players turn" 
    
    it "calls computer_move if it's the computers turn" 

  end
  
  describe "#human_move" do
    it "calls player's get_move method"
    
    it "prompts player if move is not accepted"
    
    it "marks chosen square with players mark"
  end

  describe "#computer_move" do
    it "calls computer's get_move method"
    
    it "marks chosen square with computer's mark"
  end

  describe "#game_end" do
    it "puts 'player won' message if player wins"
    
    it "puts 'computer won' message if computer wins"
    
    it "puts 'draw' message if game is a draw"
    
    it "calls play_again message"
  end

  describe "#play_again" do
    it "prompts player if they want to play again" do
      expect {
        puts <<-EOS.gsub(/^ */, '')

        Do you want to play again?
        (y/n)
        EOS
      }
    end
  end
end
